# README #


### Libopt - Parsing argument in C/C++ ###

* Usage
* Example

### Usage ###


Inspired by Python library Argparse.
```
#!c

	store_opt('v', _OPT_BOOL, &verbose);
	if (parse_opt(ac, av) < 0)
		printf("usage : ./example [-v] ...\n");
	else
	{
		[...]
	}
	# shift_opt(&ac, &av); soon;


Simply run **make** and next, link the library normally.

### Example ###

run **make example**

then **./example ...**
